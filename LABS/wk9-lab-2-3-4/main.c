#include <stdio.h>

typedef struct listNode {
    int item;
    struct listNode  *next;
} ListNode;

int searchList(ListNode *head, int value);

int main()
{
    int input;
    ListNode *head, *current;

    head = NULL;
    current = NULL;

    // question 2 ===================================================

    printf("Enter a list of numbers, terminated by the value -1: \n");
    scanf("\n");
    scanf("%d", &input);

    while (input != -1) {

        if (head == NULL) {
            // First item in list
            head = (ListNode *)malloc(sizeof(ListNode));
            current = head;
        } else {
            // setting the next node and jumping over
            current->next = (ListNode *)malloc(sizeof(ListNode));
            current = current->next;
        }
        current->item = input;
        current->next = NULL;
        scanf("%d", &input);
    }

    printf("Current list: ");
    current = head;
    while (current != NULL) {
        printf("%d ", current->item);
        current = current->next;
    }

    printf("\n");


    // question 3 ===================================================
    printf("Enter value to search for: ");
    scanf("%d", &input);
    printf("Value %d found at index %d\n", input, searchList(head, input));




    // question 4 ===================================================
    // freeing up the memory
    current = head;
    ListNode *temp;
    while (current != NULL) {
        temp = current->next;
        free(current);
        current = temp;
    }

    current = NULL;
    head = NULL;
    temp = NULL;

    return 0;
}

int searchList(ListNode *head, int value) {
    int input, counter = 0;
    ListNode *current;

    current = head;

    while (current != NULL) {
        if (current->item == value)
            return counter;
        else
            counter++;
        current = current->next;
    }

    return -1;
}
