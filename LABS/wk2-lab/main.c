#include <stdio.h>

// question 1
int computerGrade()
{
    /* insert variable declarations here */
    int studentNumber;
    int mark;

    printf("Enter Student ID: \n");
    scanf("%d", &studentNumber);

    while (studentNumber != -1)
    {
        /* Write your program code here */
        printf("Enter Mark: ");
        scanf("%d", &mark);

        switch((mark+5)/10)
        {
        case 10:
        case 9:
        case 8:
            printf("Grade = A\n");
            break;
        case 7:
            printf("Grade = B\n");
            break;
        case 6:
            printf("Grade = C\n");
            break;
        case 5:
            printf("Grade = D\n");
            break;
        default:
            printf("Grade = F\n");
            break;
        }

        printf("Enter Student ID: ");
        scanf("%d", &studentNumber);
    }
    return 0;
}

//question 2
int printAverage()
{
    int total, count, lines, input;
    double average;
    int i;
    printf("Enter number of lines: \n");
    scanf("%d", &lines);
    /* Write your program code here */
    for (int x = 0; x < lines; x++)
    {
        printf("Enter line %d (end with -1): \n", x+1);
        scanf("%d", &input);

        count = 0;
        total = 0;

        int inputArray[100];

        while (input != -1)
        {
            count++;
            total += input;
            scanf("%d", &input);
        }

        average = (double)total / count;
        printf("Average = %.2f\n", average);
    }

    return 0;
}

// question 3
int printPattern ()
{
    int row, col, height;
    int num = 0;
    printf("Enter the height: \n");
    scanf("%d", &height);
    printf("Pattern: \n");

    /* Write your program code here */
    while (num < height)
    {
        col =0;
        while (col <= num)
        {
            printf("%d", num % 3 + 1);
            col++;
        }
        printf("\n");
        num++;
    }
    return 0;
}

// question 3 with a single loop
int printPatternSingleLoop ()
{
    int row, col, height;
    int num = 0;
    printf("Enter the height: \n");
    scanf("%d", &height);
    printf("Pattern: \n");

    int count = 0;

    /* Write your program code here */
    while (num < height)
    {
        if (count <= num)
        {
            count++;
            printf("%d", num % 3 + 1);
        }
        else
        {
            num++;
            count = 0;
            printf("\n");
        }
    }
    return 0;
}

// question 3 with arrays instead of modulus +1
int printPatternArray()
{
    int row, height;
    int num = 0;
    int iterator = 1;
    int columnStep = 0;        // Check if amount of columns reached
    int targetColumn = 0;

    printf("Enter the height: \n");
    scanf("%d", &height);
    height=height+1;
    printf("Pattern: \n");

    int number[3] = {1,2,3};
    while( iterator < height)
    {

        targetColumn = iterator;
        printf("%d",number[num%3]);
        columnStep++;

        int x = 0;

        if(columnStep==targetColumn)       //Target Column amount reached
        {
            columnStep=0;
            printf("\n");
            num++;
            iterator++;
        }
    }

    return 0;
}


int main ()
{
    printPatternSingleLoop();
    return 0;
}
