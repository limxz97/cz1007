#include <stdio.h>
void printReverse1(int ar[], int size);
void printReverse2(int ar[], int size);
void reverseAr1D(int ar[], int size);
int main()
{
    int ar[80];
    int size, i;

    printf("Enter array size: \n");
    scanf("%d", &size);
    printf("Enter %d data: \n", size);
    for (i=0; i <= size-1; i++)
        scanf("%d", &ar[i]);
    printReverse1(ar, size);
    printReverse2(ar, size);
    reverseAr1D(ar, size);
    printf("reverseAr1D(): ");
    if (size > 0)
    {
        for (i=0; i<size; i++)
            printf("%d ", ar[i]);
    }
    return 0;
}
void printReverse1(int ar[], int size)
{
    /* using index � Write your program code here */
    int output;

    printf("printReverse1(): ");

    while (size > 0) {
        output = ar[--size];
        printf("%d ", output);
    }

    printf("\n");
}
void printReverse2(int ar[], int size)
{
    /* using pointer � Write your program code here */

    // Note that an array is a pointer to its first value at [0]
    // thus, *ar is a pointer th ar[0]
    //     which is also the address of the first value (aka the base of the array)
    // therefore, *(ar+1) points to ar[1]

    int output;

    printf("printReverse2(): ");

    while (size > 0) {
        output = *(ar + --size);
        printf("%d ", output);
    }

    printf("\n");
}
void reverseAr1D(int ar[ ], int size)
{
    /* Write your program code here */
    int temp;

    int counter = 0, i;

    // size is divided by 2 because i will move front AND back at the same time, so if its not divided by 2, it just reverts everything on the second half
    while (counter < size/2) {
        // store the last value in temp
        temp = *(ar + size - counter -1);

        // write the first value to the last value
        *(ar + size -counter -1) = *(ar + counter);

        // write the temp value to the first value
        *(ar + counter) = temp;

        counter++;
    }
}
