//////////////////////////////////////////////////////////////////////

// CE1007/CZ1007 Data Structures
// Week 13 Lab and Tutorial - Binary Search Tree

#include <stdio.h>
#include <stdlib.h>

///////////////////////////////////////////////////////////////////////

typedef struct _btnode
{
    int item;
    struct _btnode *left;
    struct _btnode *right;
} BTNode;

///////////////////////////////////////////////////////////////////////

void insertBSTNode(BTNode **node, int value);
void printBSTInOrder(BTNode *node);
int isBST(BTNode *node, int min, int max);
BTNode *removeBSTNode(BTNode *node, int value);
BTNode *findMin(BTNode *p);

///////////////////////////////////////////////////////////////////////

int main()
{
    int i=0;

    BTNode *root=NULL;

    //question 1
    do
    {
        printf("input a value you want to insert(-1 to quit):");

        scanf("%d",&i);
        if (i!=-1)
            insertBSTNode(&root,i);
    }
    while(i!=-1);

    //question 2
    printf("\n");
    printBSTInOrder(root);

    //question 3
    if ( isBST(root,-1000000, 1000000)==1)
        printf("It is a BST!\n");
    else
        printf("It is not a BST!\n");

    //question 4
    do
    {
        printf("\ninput a value you want to remove(-1 to quit):");
        scanf("%d",&i);
        if (i!=-1)
        {
            root=removeBSTNode(root,i);
            printBSTInOrder(root);
        }
    }
    while(i!=-1);


    return 0;
}

//////////////////////////////////////////////////////////////////////

void insertBSTNode(BTNode **node, int value)
{
    // write your code here
    if (*node == NULL)
    {
        *node = malloc(sizeof(BTNode));
        (*node)->item = value;
        (*node)->left = NULL;
        (*node)->right= NULL;
    }
    else if ((*node)->item > value)
        insertBSTNode(&((*node)->left), value);
    else if ((*node)->item < value)
        insertBSTNode(&((*node)->right), value);
    else
        printf("value already exist\n");

    return;

}

//////////////////////////////////////////////////////////////////////

void printBSTInOrder(BTNode *node)
{
    // write your code here
    if (node==NULL)
        return;

    printBSTInOrder(node->left);

    printf("%d ", node->item);

    printBSTInOrder(node->right);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int isBST(BTNode *node, int min, int max) // the item stored in node has to be smaller than max and larger than min
{
    // write your code here
    if (node==NULL)
        return 1;

    isBST(node->left, min, max);

    if (min > node->item || max < node->item)
        return 0;
    else
    {
        min = node->item;
        isBST(node->right, min, max);
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BTNode *removeBSTNode(BTNode *node, int value)
{
    // write your code here
    if (node == NULL)
    {
        printf("value not found\n");
        return NULL;
    }

    BTNode *temp;

    if (node->item > value) // go to the left node
        node->left = removeBSTNode(node->left, value);
    else if (node->item < value) // go to the left node
        node->right = removeBSTNode(node->right, value);
    else if (node->item == value)   // current node is the target
    {
        if (node->right != NULL && node->left != NULL)   //current node has 2 child
        {
            /* step 1: locate the smallest value on the right side of the tree */
            temp = findMin(node->right);
            /* step 2: transfer the left tree to the smallest node on the right tree */
            temp->left = node->left;
            /* step 3: save the address of the right node (the new head) */
            temp = node->right;
            /* step 4: free the old head */
            free(node);
            /* step 5: make the right node the new head*/
            node = temp;
        }
        else if (node->right != NULL)     // current node only has one child or no children
        {
            /* step 1: save the address of the child node (the new head) */
            if (node->left != NULL)
                temp = node->left;
            else if (node->right != NULL)
                temp = node->right;
            else
                temp = NULL;
    
            /* step 2: free the old head */
            free(node);

            /* step 3: make the right node the new head*/
            node = temp;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BTNode *findMin(BTNode *p)
{
    // write your code here
    // this function will be used for removing nodes
    if (p->left == NULL)
        return p;
    else
        findMin(p->left);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
