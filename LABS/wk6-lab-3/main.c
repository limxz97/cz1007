#include <stdio.h>
#include <string.h>
struct student
{
    char name[20]; /* student name */
    double testScore; /* test score */
    double examScore; /* exam score */
    double total; /* total = (testScore+examScore)/2 */
};
double average();
int main()
{
    printf("average(): %.2f\n", average());
    return 0;
}
double average()
{
    /* Write your program code here */

    double totalScore = 0;
    unsigned int totalPeople = 0;
    char tmpName[20];
    struct student tmp;

    printf("Enter student name: \n");
    scanf("\n");
    gets(tmpName);


    while (strcmp(tmpName, "END")) {
        strcpy(tmp.name, tmpName);

        puts("Enter test score: ");
        scanf("%lf", &tmp.testScore);

        puts("Enter exam score: ");
        scanf("%lf", &tmp.examScore);

        tmp.total = tmp.testScore + tmp.examScore;
        tmp.total/=2;

        printf("Student %s total score = %.2f\n", tmp.name, tmp.total);

        totalScore += tmp.total;
        totalPeople++;

        printf("Enter student name: \n");
        scanf("\n");
        gets(tmpName);
    }

    if (totalPeople == 0) {
        return 0;
    }
    return totalScore/totalPeople;

}
