#include <stdio.h>
#define INIT_VALUE -1000
int palindrome(char *str);
int main()
{
    char str[80];
    int result = INIT_VALUE;

    printf("Enter a string: \n");
    gets(str);
    result = palindrome(str);
    if (result == 1)
        printf("palindrome(): A palindrome\n");
    else if (result == 0)
        printf("palindrome(): Not a palindrome\n");
    else
        printf("An error\n");
    return 0;
}
int palindrome(char *str)
{
    /* Write your code here */
    unsigned int counter = 0, i;

    while (*(str+counter) != '\0') {
        counter++;
    }

    for (i = 0; i < (counter/2); i++) {
        if (*(str+counter-i-1) != *(str+i) ){
            return 0;
        }
    }

    return 1;
}
