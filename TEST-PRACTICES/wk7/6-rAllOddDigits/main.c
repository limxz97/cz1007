#include <stdio.h>
int rAllOddDigits1(int num);
void rAllOddDigits2(int num, int *result);
int main()
{
    int number, result=-1;
    printf("Enter a number: \n");
    scanf("%d", &number);
    printf("rAllOddDigits1(): %d\n", rAllOddDigits1(number));
    rAllOddDigits2(number, &result);
    printf("rAllOddDigits2(): %d\n", result);
    return 0;
}
int rAllOddDigits1(int num)
{
    /* Write your code here */
    if (num == 0)
        return 1;
    else if (num%2 != 1)
        return 0;
    else
        rAllOddDigits1(num/10);
}
void rAllOddDigits2(int num, int *result)
{
    /* Write your code here */
    if (num == 0)
        *result = 1;
    else if (num%2 != 1)
        *result = 0;
    else
        rAllOddDigits2(num/10, result);
}
