#include <stdio.h>
#include <string.h>
#define MAX 100
typedef struct
{
    char name[40];
    char telno[40];
    int id;
    double salary;
} Employee;
int readin(Employee *p);
void printEmp(Employee *p, int size) ;
int search(Employee *p, int size, char *target);
int addEmployee(Employee *p, int size, char *target);
int main()
{
    Employee emp[MAX];
    char name[40];
    int size, choice, result;

    printf("Select one of the following options: \n");
    printf("1: readin()\n");
    printf("2: search()\n");
    printf("3: addEmployee()\n");
    printf("4: print()\n");
    printf("5: exit()\n");
    do
    {
        printf("Enter your choice: \n");
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            size = readin(emp);
            break;
        case 2:
            printf("Enter search name: \n");
            scanf("\n");
            gets(name);
            result = search(emp,size,name);
            if (result != 1)
                printf ("Name not found!\n");
            break;
        case 3:
            printf("Enter new name: \n");
            scanf("\n");
            gets(name);
            result = search(emp,size,name);
            if (result != 1)
                size = addEmployee(emp, size, name);
            else
                printf("The new name has already existed in the database\n");
            break;
        case 4:
            printEmp(emp, size);
            break;
        default:
            break;
        }
    }
    while (choice < 5);
    return 0;
}
int readin(Employee *p)
{
    /* write your code here */
    char name[40];
    char telno[40];
    int id;
    double salary;

    int counter = 0;

    printf("Enter name: \n");
    scanf("\n");
    gets(name);

    while (name[0] != '#')
    {
        counter++;

        printf("Enter tel: \n");
        scanf("\n");
        gets(telno);

        printf("Enter id: \n");
        scanf("%d", &id);

        printf("Enter salary: \n");
        scanf("%lf", &salary);

        p->id = id;
        p->salary = salary;
        strcpy(p->telno, telno);
        strcpy(p->name, name);

        *p++;

        printf("Enter name: \n");
        scanf("\n");
        gets(name);
    }

    p = '\0';

    return counter;
}
void printEmp(Employee *p, int size)
{
    /* write your code here */
    unsigned int i = 0;
    printf("The current employee list: \n");
    for (i = 0; i < size; i++)
    {
        printf("%s %s %d %.2f \n", p->name, p->telno, p->id, p->salary);
        *p++;
    }
}
int search(Employee *p, int size, char *target)
{
    /* write your code here */
    int i;
    for (i = 0; i < size; i++) {
        if (strcmp(p[i].name, target) == 0) {
            printf("Employee found at index location: %d \n", i);
            printf("%s %s %d %.2f \n", p[i].name, p[i].telno, p[i].id, p[i].salary);
            return 1;
        }
    }

    return 0;
}
int addEmployee(Employee *p, int size, char *target)
{
    /* write your code here */
    if (size == MAX) {
        printf("Database is full \n");
        return size;
    }

    char telno[40];
    int id;
    double salary;

    int i;

    for (i = 0; i < size; i++) {
        *p++;
    }

    printf("Enter tel: \n");
    scanf("\n");
    gets(telno);

    printf("Enter id: \n");
    scanf("%d", &id);

    printf("Enter salary: \n");
    scanf("%lf", &salary);

    p->id = id;
    p->salary = salary;
    strcpy(p->telno, telno);
    strcpy(p->name, target);

    printf("Added at position: %d \n", size++);

    return size;
}
