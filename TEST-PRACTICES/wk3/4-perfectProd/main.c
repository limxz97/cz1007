#include <stdio.h>
int perfectProd1(int num);
void perfectProd2(int num, int *prod);
int main()
{
    int number, result=0;

    printf("Enter a number: \n");
    scanf("%d", &number);
    printf("Calling perfectProd1() \n");
    printf("perfectProd1(): %d\n", perfectProd1(number));

    printf("Calling perfectProd2() \n");
    perfectProd2(number, &result);
    printf("perfectProd2(): %d\n", result);
    return 0;
}
int perfectProd1(int num)
{
    /* Write your code here */
    int sum = 0, counter = 0;
    int output = 1;
    int i;

    for (counter = 1; counter < num; counter++) {
        sum = 0;
        for (i = 1; i < counter; i++) {

            // i is a factor of counter
            if (counter%i == 0)
                sum += i;
        }


        // counter is a perfect number
        if (counter == sum) {
            output *= counter;
            printf("Perfect number: %d \n", counter);
        }
    }

    return output;
}
void perfectProd2(int num, int *prod)
{
    /* Write your code here */
    int sum = 0, counter = 0;
    int i;

    *prod = 1;

    for (counter = 1; counter < num; counter++) {
        sum = 0;
        for (i = 1; i < counter; i++) {

            // i is a factor of counter
            if (counter%i == 0)
                sum += i;
        }


        // counter is a perfect number
        if (counter == sum) {
            *prod *= counter;
            printf("Perfect number: %d \n", counter);
        }
    }
}
