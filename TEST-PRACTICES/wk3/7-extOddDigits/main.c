#include <stdio.h>
#define INIT_VALUE 999
int extOddDigits1(int num);
void extOddDigits2(int num, int *result);
int main()
{
    int number, result = INIT_VALUE;

    printf("Enter a number: \n");
    scanf("%d", &number);
    printf("extOddDigits1(): %d\n", extOddDigits1(number));
    extOddDigits2(number, &result);
    printf("extOddDigits2(): %d\n", result);
    return 0;
}
int extOddDigits1(int num)
{
    /* Write your program code here */
    int output = 0, pos = 1;
    int i;

    while (num > 0) {
        // true if num is a odd number
        if (num%2) {
            output += (num%10 * pos);
            pos *= 10;
        }
        num /= 10;
    }

    output = (output == 0) ? -1 : output;

    return output;
}
void extOddDigits2(int num, int *result)
{
    /* Write your program code here */
    int pos = 1;
    int i;

    *result = 0;

    while (num > 0) {
        // true if num is a odd number
        if (num%2) {
            *result = *result + (num%10 * pos);
            pos *= 10;
        }
        num /= 10;
    }

    *result = (*result == 0) ? -1 : *result;
}
