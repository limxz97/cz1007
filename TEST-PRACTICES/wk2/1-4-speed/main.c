#include <stdio.h>
int main()
{
    /* Write your program code here */

    double distance, time, speed;

    printf("Enter distance (in km): \n");
    scanf("%lf", &distance);

    printf("Enter time (in sec): \n");
    scanf("%lf", &time);

    speed = distance / time;

    printf("The speed is %.2f km/sec \n", speed);

    return 0;
}
