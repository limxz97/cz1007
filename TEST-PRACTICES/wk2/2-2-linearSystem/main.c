#include <stdio.h>
#include <math.h>
int main()
{
    /* Write your program code here */
    double a1,b1,c1,a2,b2,c2, x, y;

    printf("Enter the values for a1, b1, c1, a2, b2, c2: \n");
    scanf("%lf %lf %lf %lf %lf %lf", &a1, &b1, &c1, &a2, &b2, &c2);

    if ((a1 * b2) == (a2 * b1)) {
        printf("Unable to compute because the denominator is zero! \n");
        return 0;
    }

    x = (b2 * c1 - b1 * c2) / (a1 * b2 - a2 * b1);
    y = (a1 * c2 - a2 * c1) / (a1 * b2 - a2 * b1);

    printf("x = %.2f and y = %.2f", x, y);
    return 0;
}
