#include <stdio.h>
#define PI 3.1416
int main()
{
    /* Write your program code here */
    double radius, height, vol, surface;
    printf("Enter the radius: \n");
    scanf("%lf", &radius);

    printf("Enter the height: \n");
    scanf("%lf", &height);

    vol = PI * radius * radius * height;
    surface = 2 * PI * radius * height + 2 * PI * radius * radius;

    printf("The volume is: %.2f \n", vol);
    printf("The surface area is: %.2f \n", surface);

    return 0;
}
